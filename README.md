# Serverless Rust Microservice
This project contains a Rust-based AWS Lambda function designed to record student attendance in a DynamoDB table. The Lambda function utilizes the `aws_sdk_dynamodb` crate for DynamoDB interactions, `aws_config` for loading AWS configuration, chrono for working with time, and `lambda_runtime` for Lambda function setup.
## Rust Code Overview 
### 1. Request Struct
The `Request` struct represents the payload format expected by the Lambda function. It includes the student's `id`, `name`, and `course_id`.
### 2. LambdaResponse Struct
The `LambdaResponse` struct is used to structure the response from the Lambda function. It includes a success message and the count of course attendance.
### 3. Lambda Function Handler
The `lambda_handler` function is the entry point for the Lambda function. It deserializes the Lambda event payload, `records attendance` using the record_attendance function, and constructs a response with the attendance count.
### 4. Record Attendance Function
The `record_attendance` function is responsible for recording attendance in the DynamoDB table. It uses the `aws_sdk_dynamodb` crate to perform a `put_item` operation and then calls the `count_course` function to get the attendance count.
### 5. Count Course Function
The `count_course` function queries the DynamoDB table to count the number of course attendance records for a given student. It utilizes the `scan` operation and constructs a response based on the retrieved items.
### 6. Running the Lambda Function
To run the Lambda function, the `main` function sets up the Lambda handler and initiates the Lambda runtime using `lambda_runtime::run(func)`.

Make sure to replace placeholder values in the code, such as the DynamoDB table name and AWS environment variables, with your actual configuration.
### 7. Main Dependencies
* `aws_sdk_dynamodb`: AWS SDK for Rust DynamoDB support.
* `aws_config`: AWS configuration loading for Rust.
* `chrono`: Date and time handling in Rust.
* `lambda_runtime`: AWS Lambda runtime for Rust.

## Project Deployment
* Clone the project to the local repository and install rust. 
* Run `cargo lambda build --release` to build the rust application. 
* Run `aws configure` to add your AWS access key, make sure to install AWS CLI `brew install awscli`.
* Run `cargo lambda deploy` to deploy your application to the AWS Lambda. 
* Add an API Gateway for website access.

![Screenshot 2024-03-01 at 12.27.32 PM.png](screenshot%2FScreenshot%202024-03-01%20at%2012.27.32%20PM.png)

## DynamoDB Database Configuration
To create an AWS DynamoDB, you can sign up to your AWS cloud console and create a database table called `attendance_records`. 

![Screenshot 2024-03-01 at 12.26.45 PM.png](screenshot%2FScreenshot%202024-03-01%20at%2012.26.45%20PM.png)

It is also required to add some permissions to the IAM role you used. All the permissions are `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess` and `AWSLambdaBasicExecutionRole`. This allows you to get full access (read/write) to the database system.

![Screenshot 2024-03-01 at 12.35.50 PM.png](screenshot%2FScreenshot%202024-03-01%20at%2012.35.50%20PM.png)

## Testing
You can use the `Test event` function in AWS Lambda to test the service you built. Make sure that the Event JSON should be in the form:
```json
{
  "id": "value1",
  "name": "value2",
  "course_id": 123
}
```

![Screenshot 2024-03-01 at 12.28.27 PM.png](screenshot%2FScreenshot%202024-03-01%20at%2012.28.27%20PM.png)

You can see the returned information in the following screenshot. Note that the message indicates successful check-in, and the course check-in count is 1:

![Screenshot 2024-03-01 at 12.28.49 PM.png](screenshot%2FScreenshot%202024-03-01%20at%2012.28.49%20PM.png)

We can see various items in the database table based on the following screenshot, every item consists of `student_attendance` (primary key), `course_id`, `id` (student id), `name` (student name) and `timestamp` (attendance time):

![Screenshot 2024-03-01 at 12.27.12 PM.png](screenshot%2FScreenshot%202024-03-01%20at%2012.27.12%20PM.png)

Alternatively, you can access the service using the website `https://zeus2t78z0.execute-api.us-east-1.amazonaws.com/xc202/` as the API Gateway has been configured:
```shell
curl -X POST \
  -H "Content-Type: application/json" \
  -d '{
    "id": "value1",
    "name": "value2",
    "course_id": 123
  }' \
  https://zeus2t78z0.execute-api.us-east-1.amazonaws.com/xc202/
```

![Screenshot 2024-03-01 at 12.29.23 PM.png](screenshot%2FScreenshot%202024-03-01%20at%2012.29.23%20PM.png)